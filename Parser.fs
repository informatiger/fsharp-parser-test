﻿module Parser
    open FParsec
    type ParserState = unit

    ///<summary>
    ///Operations of the AoC Language
    ///</summary>
    type Operation =
        | GoUp
        | Go2Up
        | GoDown
        | Go2Down

    ///<summary>
    ///Parses the GoUp
    ///</summary>
    let pgoup : Parser<Operation,ParserState> = 
        pchar '(' >>% GoUp

    ///<summary>
    ///Parses the Go2Up
    ///</summary>
    let pgo2up : Parser<Operation,ParserState> = 
        pchar '<' >>% Go2Up

    ///<summary>
    ///Parses the GoDown
    ///</summary>
    let pgodown: Parser<Operation, ParserState> =
        pchar ')' >>% GoDown

    ///<summary>
    ///Parses the Go2Down
    ///</summary>
    let pgo2down: Parser<Operation, ParserState> =
        pchar '>' >>% Go2Down

    ///<summary>
    ///Master Parser
    ///</summary>
    let parseOperation = choice [pgoup; pgo2up; pgodown; pgo2down]

    ///<summary>
    ///Perform command
    ///</summary>
    let perform op (floor: int): int =
        match op with
        | GoUp -> 
            floor + 1
        | Go2Up ->
            floor + 2
        | GoDown -> 
            floor - 1
        | Go2Down ->
            floor - 2
