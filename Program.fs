﻿// Learn more about F# at http://fsharp.org

open System
open FParsec

open Parser

[<EntryPoint>]
let main _ =
    let data  = Seq.toList "<))((((()()))()()()((())()()()()))(((()()()()))))((()()(>"

    let mutable floor = 0
    let runOperation op =
        match run parseOperation op with
        | Success(result, _, _)   -> floor <- perform result floor
        | Failure(errorMsg, _, _) -> failwith errorMsg

    data |> List.map (fun op -> runOperation (string op)) |> ignore
   
    printfn "Target Floor: %d" floor

    0
